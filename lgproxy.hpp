#ifndef LGPROXY_HPP
#define LGPROXY_HPP

#include "lg.hpp"
#include <vector>

class LGProxy
{
public:
    void addContext(LG::Context* c)
    {
        ctxs.push_back(c);
    }

    void clear() { for(auto* i : ctxs) { i->clear(); } }
    void present() {for( auto* i : ctxs) { i->present(); } }
    void laserOn() { for(auto* i : ctxs) { i->laserOn(); } }
    void laserOff() { for(auto* i : ctxs) { i->laserOff(); } }

    void setViewPort(const uint32_t x, const uint32_t y) { for(auto* i : ctxs) { i->setViewPort(x,y); } }
    void setVertexFunction(LG::VertexFunction* vf) { for(auto* i : ctxs) { i->setVertexFunction(vf); } }
private:
    std::vector<LG::Context*> ctxs;

};


#endif // LGPROXY_HPP

#define GLM_ENABLE_EXPERIMENTAL

#include "lg.hpp"
#include "sdlcontext.hpp"

#include <chrono>
#include <vector>
#include <cstdint>

#include <box2d/box2d.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/transform2.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_gamecontroller.h>

#include "player.hpp"
#include "ball.hpp"
#include "net.hpp"



class VertexShader : public LG::VertexFunction
{
public:
    VertexShader()
    {
        mv = glm::mat4(1.0f);
        pj = glm::ortho(0.0f,1.0f,1.0f,0.0f);
        //pj = glm::perspective(45.0f, 1.0f, 0.2f, 1000.0f);

    }

    virtual glm::vec4 operator() (const glm::vec4& in)
    {
        return pj * mv * in;
    }

    glm::mat4 mv, pj;
};



int main(int argc, char** argv)
{
    SDL_Init(SDL_INIT_EVERYTHING);

    std::vector<std::pair<SDL_GameController*, int>> controllers;
    for(int i = 0; i < SDL_NumJoysticks(); ++i)
    {
        if(SDL_IsGameController(i))
        {
            auto* c = SDL_GameControllerOpen(i);
            if(c) { controllers.push_back({c,i}) ; }
        }
    }


    b2Vec2 gravity(0.0f,9.81f / 2.0f);
    b2World world(gravity);

    Player p1(world, 0.06f, 255,0,0); p1.setPosition(0.2,0.8);
    Player p2(world, 0.06f,0,0,255); p2.setPosition(0.8,0.8);
    Ball ball(world, 24, 0.02f,255,255,255);
    Net net(world,  0,255,0);




    VertexShader *fft = new VertexShader;
    SDLLG c;
    c.setVertexFunction(fft);
    c.setViewPort(256,256);



    SDL_Event e;
    bool quit = false;
    while (!quit)
    {

        static auto tp1 = std::chrono::high_resolution_clock::now();
        static auto tp2 = std::chrono::high_resolution_clock::now();





        while( SDL_PollEvent( &e ) )
        {
            //User requests quit
            if( e.type == SDL_QUIT )
            {
                quit = true;
            }
            else if(e.type == SDL_CONTROLLERDEVICEADDED || e.type == SDL_CONTROLLERDEVICEREMOVED)
            {
                for(auto& i : controllers) { SDL_GameControllerClose(i.first); }
                controllers.clear();
                for(int i = 0; i < SDL_NumJoysticks(); ++i)
                {
                    if(SDL_IsGameController(i))
                    {
                        auto* c = SDL_GameControllerOpen(i);
                        if(c) { controllers.push_back({c,i}) ; }
                    }
                }
            }
            else if(e.type == SDL_KEYDOWN)
            {
                if(e.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
                {
                    quit = true;
                }
            }
        }

        if(controllers.size() >= 1)
        {
            p1.movingRight = SDL_GameControllerGetButton(controllers[0].first, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
            p1.movingLeft = SDL_GameControllerGetButton(controllers[0].first, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_LEFT);
            p1.pressedA = SDL_GameControllerGetButton(controllers[0].first, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_A);
        }
        if(controllers.size() >= 2)
        {
            p2.movingRight = SDL_GameControllerGetButton(controllers[1].first, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_RIGHT);
            p2.movingLeft = SDL_GameControllerGetButton(controllers[1].first, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_DPAD_LEFT);
            p2.pressedA = SDL_GameControllerGetButton(controllers[1].first, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_A);

        }



        tp2 = std::chrono::high_resolution_clock::now();
        if(std::chrono::duration_cast<std::chrono::milliseconds>(tp2-tp1).count() >= 16)
        {

            //game logic scoring, win/lose
            p1.update();
            p2.update();


            //world update
            world.Step(1.0f/60.0f, 12, 5);



            //Rendering
            c.clear();


            //draw borders
            fft->mv = glm::mat4(1.0f);
            c.ColorPointer(net.colors.data());
            c.VertexPointer(2, net.borderVerts.data());
            c.DrawArray(LG::DrawType::Line_Loop, 0, net.borderVerts.size());

            //draw net
            //fft->mv = glm::translate(glm::mat4(1.0f), {net.position.x, net.position.y, 0.0f});
            c.ColorPointer(net.colors.data());
            c.VertexPointer(2, net.verts.data());
            c.DrawArray(LG::DrawType::Line_Loop, 0, net.verts.size());



            //draw ball
            fft->mv = glm::translate(glm::mat4(1.0f), {ball.ballBody->GetPosition().x, ball.ballBody->GetPosition().y, 0.0f});
            c.ColorPointer(ball.colors.data());
            c.VertexPointer(2, ball.verts.data());
            c.DrawArray(LG::DrawType::Line_Loop, 0, ball.verts.size());

            //draw p1
            fft->mv = glm::translate(glm::mat4(1.0f), {p1.body->GetPosition().x, p1.body->GetPosition().y, 0.0f});
            c.ColorPointer(p1.colors.data());
            c.VertexPointer(2, p1.verts.data());
            c.DrawArray(LG::DrawType::Line_Loop, 0, p1.verts.size());

            //draw p2
            fft->mv = glm::translate(glm::mat4(1.0f), {p2.body->GetPosition().x, p2.body->GetPosition().y, 0.0f});
            c.ColorPointer(p2.colors.data());
            c.VertexPointer(2, p2.verts.data());
            c.DrawArray(LG::DrawType::Line_Loop, 0, p2.verts.size());

            c.present();
            tp1 = std::chrono::high_resolution_clock::now();

        }






    }

    return 0;
}

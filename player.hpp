#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "util.hpp"
#include "input.hpp"
#include <box2d/box2d.h>

class Player
{
public:
    static constexpr float SPEED = 1.0f;
    static constexpr float JUMP = 0.2f;

    Player(b2World& w, const float rad, const uint8_t r, const uint8_t g, const uint8_t b)
    {
        radius = rad;
        verts = createDome(12, radius);
        colors = createColors(r,g,b, verts.size());

        bodyDef.type = b2_dynamicBody;
        bodyDef.fixedRotation = true;
        body = w.CreateBody(&bodyDef);

        shape.Set((b2Vec2*)verts.data(), verts.size());

        b2FixtureDef fixtureDef;
        fixtureDef.shape = &shape;
        fixtureDef.density = 1.0f;
        fixtureDef.restitution = 0.0f;
        //fixtureDef.friction = 0.0f;
        body->CreateFixture(&fixtureDef);

    }

    void setPosition(const float x, const float y) { body->SetTransform({x,y}, 0.0f); }

    void update()
    {


        if(movingLeft)
        {
            body->ApplyLinearImpulse({-MOVESPEED,0.0f}, body->GetPosition(), true);

        }
        if(movingRight)
        {
            //body->SetLinearVelocity({MOVESPEED,0.0f});
            body->ApplyLinearImpulse({MOVESPEED,0.0f}, body->GetPosition(), true);
        }
        if(pressedA)
        {
            if(body->GetLinearVelocity().y >= 0.0f)
            {
            body->ApplyLinearImpulse({0.0f,-0.975f}, body->GetPosition(), true);
            }

        }


        if(body->GetLinearVelocity().x > SPEED)
        {
            const auto t = body->GetLinearVelocity();
            body->SetLinearVelocity( {SPEED, t.y} );
        }
        if(body->GetLinearVelocity().x < -SPEED)
        {
            const auto t = body->GetLinearVelocity();
            body->SetLinearVelocity( {-SPEED, t.y} );
        }


    }

    const float MOVESPEED = 9.0f;
    unsigned int p = 0;
    float radius;

    bool movingLeft = false, movingRight = false, jumping = false, pressedA = false;

    std::vector<glm::vec2> verts;
    std::vector<LG::Context::Color> colors;

    b2BodyDef bodyDef;
    b2Body* body;
    b2PolygonShape shape;
};

#endif // PLAYER_HPP

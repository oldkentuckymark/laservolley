#ifndef INPUT_HPP
#define INPUT_HPP

#pragma once

#include <cstdint>

class Inputs
{
public:
    static const uint8_t A = 0;
    static const uint8_t B = 1;
    static const uint8_t X = 2;
    static const uint8_t Y = 3;
    static const uint8_t BACK = 4;
    static const uint8_t GUIDE = 5;
    static const uint8_t START = 6;
    static const uint8_t LS = 7;
    static const uint8_t RS = 8;
    static const uint8_t LT = 9;
    static const uint8_t RT = 10;
    static const uint8_t UP = 11;
    static const uint8_t DOWN = 12;
    static const uint8_t LEFT = 13;
    static const uint8_t RIGHT = 14;

    Inputs()
    {
        clear();
    }

    void set(const uint8_t b)
    {
        prev_[b] = buttons_[b];
        buttons_[b] = true;
    }

    void unset(const uint8_t b)
    {
        prev_[b] = buttons_[b];
        buttons_[b] = false;
    }

    bool held(const uint8_t b) const
    {
        return buttons_[b];
    }

    bool pressed(const uint8_t b) const
    {
        return buttons_[b] && !prev_[b];
    }

    void clear()
    {
        for(auto i = 0ul; i < 15; ++i)
        {
            prev_[i] = false;
            buttons_[i] = false;
        }
    }


private:
    bool buttons_[15];
    bool prev_[15];

};



#endif // INPUT_HPP

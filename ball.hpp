#ifndef BALL_HPP
#define BALL_HPP

#include <box2d/box2d.h>
#include "util.hpp"

class Ball
{
public:
    Ball(b2World& w, const int subdiv, const float rad, const uint8_t r, const uint8_t g, const uint8_t b)
    {
        radius = rad;
        verts = createCircle(subdiv, rad);
        colors = createColors(r,g,b, verts.size());

        ballBodyDef.type = b2_dynamicBody;
        ballBodyDef.fixedRotation = true;
        ballBodyDef.position = {0.5f,0.5f};
        ballBody = w.CreateBody(&ballBodyDef);

        ballShape.m_radius = radius;

        b2FixtureDef fixtureDef;
        fixtureDef.shape = &ballShape;
        fixtureDef.density = 1.0f;
        //fixtureDef.density = 1.0f;
        fixtureDef.restitution = 0.0f;
        //fixtureDef.friction = 0.001f;
        ballBody->CreateFixture(&fixtureDef);
        ballBody->SetLinearVelocity({-0.6f,-1.0f});


    }

    float radius;
    std::vector<glm::vec2> verts;
    std::vector<LG::Context::Color> colors;

    b2BodyDef ballBodyDef;
    b2Body* ballBody;
    b2CircleShape ballShape;

    void setPosition(const float x, const float y) {ballBody->SetTransform({x,y},0.0f);}
    void setVelocity(const float x, const float y){ballBody->SetLinearVelocity({x,y});}
};


#endif // BALL_HPP

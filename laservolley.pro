CONFIG += c++17

HEADERS += \
	ball.hpp \
	input.hpp \
	lasercontext.hpp \
	ld.hpp \
	lg.hpp \
	lgproxy.hpp \
	net.hpp \
	player.hpp \
	sdlcontext.hpp \
	util.hpp

SOURCES += \
	main.cpp

LIBS += -lSDL2 -lbox2d

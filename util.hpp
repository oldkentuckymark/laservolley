#ifndef UTIL_HPP
#define UTIL_HPP

#include <vector>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include "lg.hpp"

const float MTP = 30.0f;
const float PTM = 1.0f/30.0f;

std::vector<glm::vec2> createCircle(const unsigned int n, const float radius = 1.0f)
{
    std::vector<glm::vec2> verts;
    const float increm = ((glm::pi<float>()) * 2.0f) / n;
    for(auto i = 0ul; i < n; ++i)
    {
        verts.emplace_back(glm::cos(i*increm)*radius, glm::sin(i*increm)*radius);
    }
    return verts;
}

std::vector<glm::vec2> createDome(const unsigned int n, const float radius = 1.0f)
{
    std::vector<glm::vec2> verts;
    const float increm = ((glm::pi<float>()) * 2.0f) / n;
    for(auto i = 0ul; i < (n/2) + 1; ++i)
    {
        verts.emplace_back(glm::cos(i*increm)*radius, -glm::sin(i*increm)*radius);
    }

    return verts;
}

std::vector<glm::vec2> createBox(const glm::vec2 size)
{
    std::vector<glm::vec2> verts;
    verts.emplace_back(-size.x,size.y);
    verts.emplace_back(-size.x,-size.y);
    verts.emplace_back(size.x,-size.y);
    verts.emplace_back(size.x,size.y);
    return verts;
}

std::vector<LG::Context::Color> createColors(const uint8_t r, const uint8_t g, const uint8_t b, const uint8_t num)
{
    std::vector<LG::Context::Color> colors;

    for(auto i = 0ul; i < num; ++i)
    {
        colors.push_back({r,g,b,255});
    }

    return colors;
}


#endif // UTIL_HPP

#ifndef SDLCONTEXT_HPP
#define SDLCONTEXT_HPP

#include <cstdint>
#include "lg.hpp"
#include <SDL2/SDL.h>

class SDLLG : public LG::Context
{
public:
    SDL_Window* window;
    SDL_Renderer* renderer;
    uint8_t cr,cg,cb;
    bool laser_on = false;
    unsigned int scale = 3;

    SDLLG()
    {
        window = SDL_CreateWindow("laservolley", 250,250,256*scale,256*scale, 0);
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);


    }
    ~SDLLG()
    {
        SDL_DestroyWindow(window);
        SDL_DestroyRenderer(renderer);
        SDL_Quit();
    }
    virtual void laserColor(const uint8_t r, const uint8_t g, const uint8_t b)
    {
        cr = r;
        cb = b;
        cg = g;
        SDL_SetRenderDrawColor(renderer, cr,cg,cb,255);
    }
    virtual void laserMove(const uint32_t x, const uint32_t y)
    {
        static uint32_t px, py;
        if(laser_on) { SDL_RenderDrawLine(renderer, px*scale,py*scale, x*scale,y*scale); }
        px = x;
        py = y;
    }
    virtual void laserOn()
    {
        laser_on = true;
        SDL_SetRenderDrawColor(renderer, cr,cg,cb,255);
    }
    virtual void laserOff()
    {
        laser_on = false;
        SDL_SetRenderDrawColor(renderer, 255,0,0,255);
    }
    virtual void clear()
    {
        SDL_SetRenderDrawColor(renderer, 0,0,0,255);
        SDL_RenderClear(renderer);
    }
    virtual void present()
    {
        SDL_RenderPresent(renderer);
    }

};

#endif // SDLCONTEXT_HPP

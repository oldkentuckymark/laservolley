#ifndef NET_HPP
#define NET_HPP

#include <box2d/box2d.h>
#include "util.hpp"

class Net
{
public:
    Net(b2World& w, const uint8_t r, const uint8_t g, const uint8_t b)
    {
        glm::vec2 size = {0.01f,0.1f};
        glm::vec2 positon = {0.5f,0.9f};

        verts = createBox(size);
        for(auto& i : verts) i += positon;

        borderVerts.push_back({0.0f,0.0f});
        borderVerts.push_back({255.0f/256.0f,0.0f});
        borderVerts.push_back({255.0f/256.0f,255.0f/256.0f});
        borderVerts.push_back({0.0f,255.0f/256.0f});



        colors = createColors(r,g,b, verts.size());

        arenaBodyDef.type = b2_staticBody;
        arenaBodyDef.position.Set(0.0f, 0.0f);
        arenaBody = w.CreateBody(&arenaBodyDef);

        bottomShape.SetTwoSided({0.0f,1.0f}, {1.0f,1.0f} );
        topShape.SetTwoSided({0.0f,0.0f},{1.0f,0.0f});
        leftShape.SetTwoSided({0.0f,0.0f},{0.0f,1.0f});
        rightShape.SetTwoSided({1.0f,0.0f},{1.0f,1.0f});

        bottomDef.friction = 0.0f;
        bottomDef.shape = &bottomShape;
        arenaBody->CreateFixture(&bottomDef);
        arenaBody->CreateFixture(&topShape, 0.0f);

        leftDef.friction = 0.0f;
        leftDef.restitution = 0.0f;
        leftDef.shape = &leftShape;
        arenaBody->CreateFixture(&leftDef);

        rightDef.friction = 0.0f;
        rightDef.restitution = 0.0f;
        rightDef.shape = &rightShape;
        arenaBody->CreateFixture(&rightDef);




        bottomNetShape.SetTwoSided( {verts[3].x, verts[3].y}, {verts[0].x, verts[0].y} );
        leftNetShape.SetTwoSided(  {verts[0].x, verts[0].y}, {verts[1].x, verts[1].y}  );
        rightNetShape.SetTwoSided(  {verts[2].x, verts[2].y}, {verts[3].x, verts[3].y}  );
        topNetShape.SetTwoSided(  {verts[1].x, verts[1].y}, {verts[2].x, verts[2].y}  );

        arenaBody->CreateFixture(&bottomNetShape, 0.0f);

        leftNetDef.friction = 0.0f;
        leftNetDef.restitution = 0.5f;
        leftNetDef.shape = &leftNetShape;
        arenaBody->CreateFixture(&leftNetDef);


        rightNetDef.friction = 0.0f;
        rightNetDef.restitution = 0.5f;
        rightNetDef.shape = &rightNetShape;
        arenaBody->CreateFixture(&rightNetDef);

        topNetDef.friction = 0.0f;
        topNetDef.restitution = 0.5f;
        topNetDef.shape = &topNetShape;
        arenaBody->CreateFixture(&topNetDef);








    }

    void setPosition(const float x, const float y) {arenaBody->SetTransform({x,y},0.0f);}
    void setVelocity(const float x, const float y){arenaBody->ApplyForceToCenter({x,y},true);}

    std::vector<glm::vec2> verts;
    std::vector<glm::vec2> borderVerts;
    std::vector<LG::Context::Color> colors;

    b2BodyDef arenaBodyDef;
    b2Body* arenaBody;


    b2EdgeShape bottomShape, leftShape, rightShape, topShape;
    b2FixtureDef leftDef, rightDef, bottomDef;

    b2EdgeShape bottomNetShape, leftNetShape, rightNetShape, topNetShape;
    b2FixtureDef leftNetDef, rightNetDef, bottomNetDef, topNetDef;



};

#endif // NET_HPP
